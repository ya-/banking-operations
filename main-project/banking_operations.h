#ifndef BANKING_OPERATIONS_H
#define BANKING_OPERATIONS_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};

struct time
{
    int hour;
    int minut;
    int second;
};
 
struct banking_operation
{
    date date1;
    time time1;
    char type[MAX_STRING_SIZE];
    char invoice[MAX_STRING_SIZE];
    float sum;
    char purpose[MAX_STRING_SIZE];
};

#endif
